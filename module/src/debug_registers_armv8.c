/* 
 * Author: Luis Fueris Mart�n
 * Date: March 12, 2019
 * Filename: debug_registers_armv8.c
 * Documentation: ARM Architecture Reference Manual ARMv8, for ARMv8-A 
 * architecture profile - Chapter D2 AArch64 Self-Hosted Debug.
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

int init_anorexic_mod(void) 
{
    printk(KERN_INFO "[debug_registers] Starting the registers "
                                                    "initialization\n");
    /* in AArch64 the OS lock is writable via oslar register */
    __asm__("MOV    x2, #1");
    __asm__("MSR    oslar_el1, x2");
    /* context synchronization event */
    __asm__("ISB");

    /* read break control register no. 0 */
    __asm__("MRS    x3, dbgbcr0_el1");
    __asm__("BIC    x3, x3, x3");
    /* HMC = 0, SSC = 00, PMC = 01. These states define the breakpoint 
        execution conditions. In this case we define PMC (table D2-10) */
    __asm__("ORR    x3, x3, #(1 << 1)"); 

    /* write beakpoint control register flag to enable breakpoint no. 0 */
    __asm__("ORR    x3, x3 #(1 << 0)");
    __asm__("MSR    dbgbcr0_el1, x3");

    /* read breakpoint value register no 0 */
    __asm__("MRS    x4, dbgbvr0_el1");
    /* write kmalloc virtual address, if you want to obtain this address 
        please check System.map file in /boot directory */
    __asm__("LDR    x5, =0xffffffc0001b2d20");
    __asm__("MOV    x4, x5");
    __asm__("MSR    dbgbvr0_el1, x4");

    /* context synchronization event */
    __asm__("ISB");
    /* clear OS lock */
    __asm__("MOV    x2, #0");
    __asm__("MSR    oslar_el1, x2");
    
    /* a final isb instruction guarantees the restores register values are
        visible to subsequent instructions */
    __asm__("ISB");

    printk(KERN_INFO "[debug_registers] Ending the registers "
                                                    "initialization\n");
    return 0;
}

void exit_debug_registers(void) 
{
    printk(KERN_INFO "[debug_registers] Bye...\n"); 

    /* in AArch64 the OS lock is writable via oslar register */
    __asm__("mov x2, #1");
    __asm__("msr oslar_el1, x2");
    /* context synchronization event */
    __asm__("isb");

    /* disable breakpoint control register */
    __asm__("mrs x3, dbgbcr0_el1");
    __asm__("bic x3, x3, x3");
    __asm__("orr x3, x3, #(0 << 0)");
    __asm__("msr dbgbcr0_el1, x3");

    /* TODO: clean breakpoint value register no. 0 ? */

}

module_init(init_debug_registers);
module_exit(exit_debug_registers);
MODULE_LICENSE("GPL");
