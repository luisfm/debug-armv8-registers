# Proof of Concept - Debug ARMv8 Registers 

This example show how to use ARMv8 debug register in order to trace a kernel function. There are two important debug register.

* Breakpoint debug control register ``dbgbcrX_el1`` which enables and have the control of debugging.
* Breakpoint debug value register ``dbgbvrX_el1`` in which we need to write function address and how it does the match

If you want more information about debug registers, please check [ARM Architecture Reference Manual ARMv8, for ARMv8-A architecture profile - Chapter D2 AArch64 Self-Hosted Debug](https://developer.arm.com/docs/ddi0487/latest/arm-architecture-reference-manual-armv8-for-armv8-a-architecture-profile).